%\documentclass[12pt,a4paper]{article}
\documentclass[american,a4paper]{article}
\usepackage{float}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{amssymb}

\lstset{language=C++, basicstyle=\ttfamily,
  keywordstyle=\color{black}\bfseries, tabsize=4, stringstyle=\ttfamily,
  commentstyle=\it, extendedchars=true, escapeinside={/*@}{@*/}}

\title{\textbf{DUNE-PDELab Exercise 1\\Wednesday, Feb~25\textsuperscript{th}~2015 \\11:15-12:45}}
\dozent{Pavel Hron, Adrian Ngo}
\institute{IWR, University of Heidelberg}
\semester{}
\abgabetitle{}
%\abgabeinfo{ in der Übung}
\uebungslabel{Exercise}
\blattlabel{DUNE Workshop 2015 Exercise}


\newcommand{\vx}{\vec x}
\newcommand{\grad}{\vec \nabla}
\newcommand{\wind}{\vec \beta}
\newcommand{\Laplace}{\Delta}
\newcommand{\mycomment}[1]{}

\begin{document}

\blatt{}{}

\begin{uebung}{\bf Stationary problems without constraints}

  In this session you will
  \begin{itemize}
  \item work on the problems presented in the lecture and see how the
    code works
  \item extend the implementation of the local operator by a non-linear
    term
  \item add a space-dependent diffusion coefficient to the local
    operator
  \end{itemize}

  \begin{enumerate}
  \item {\sc Warming up}:\\ {\bf Example 1a)} The following elliptic
    equation with natural boundary conditions
    $$
    \begin{array}{rclc}
      -\Laplace u + a u & = & f & \text{ in } \Omega \\ \grad u \cdot
      \vec\nu & = & 0 & \text{ on } \partial\Omega=\Gamma_N
    \end{array}
    $$

    is specified by $a=10$ and by the source term

    $$ f(\vx) := \left\{
    \begin{array}{rl}
      -10 & \mbox{ for $\|\vx\|_2 < \frac{1}{4}$} \\ 10 & \mbox{ else }
    \end{array}
    \right.
    $$

    The files containing the code to discretize and solve this model
    problem with $Q_1$, $Q_2$ and Rannacher-Turek finite elements are listed in the lecture notes of the pdelab.
    %in page 11 of the pdelab lecture notes.
    {\bf Get familiar with the file {\tt example01.cc} and all the {\tt
        example01a\_*.hh} files.  Try to understand how the code in the
      files interacts!}  To build and execute this example on a
    $2^3\times2^3$-grid use the commands

\lstset{language=bash}
\begin{lstlisting}
  dune001@cip54:~$ cd pdelab-exercise1/debug-build/src
  dune001@cip54:~/pdelab-exercise1/debug-build/src$ make example01
  dune001@cip54:~/pdelab-exercise1/debug-build/src$ ./example01 3
\end{lstlisting}

    Rerun this example on a finer grid, i.e. a
    $2^6\times2^6$-grid. Switch to the \lstinline!release-build!
    directory and execute the example with the same settings using the
    following commands

\begin{lstlisting}
  dune001@cip54:~/pdelab-exercise1/debug-build/src$ cd ../../release-build/src
  dune001@cip54:~/pdelab-exercise1/release-build/src$ make example01
  dune001@cip54:~/pdelab-exercise1/release-build/src$ ./example01 6
\end{lstlisting}

    In the \lstinline!release-build! the program is compiled with
    optimizations resulting in longer compilation time, but the program
    is much faster. \newline
    It is up to you whether you are going to continue the exercise in
    the \lstinline!debug-build! or in the \lstinline!release-build!, but
    we recommend doing it in the latter directory since the program is
    much more faster.

In the paraview user interface you must press {\tt Apply} to see the
loaded {\tt .vtu}-file. To see a 3d surface of a scalar function
defined on a 2d domain, use the {\tt Warp By Scalar} filter. {\bf
  Compare the three solutions to see the differences between the
  finite elements.}

\vspace{5mm}
\item {\sc Nonlinearity}:\\ {\bf Example 1b)} Now we add a
  second-order term $u^2$ to make our PDE non-linear:
  $$
  \begin{array}{cccc}
    -\Laplace u + a (u + u^2) & = & f & \text{ in } \Omega \\
  \end{array}
  $$ with $a$ and $f$ as specified above.

  The template class inside {\tt example01b\_operator.hh} requires a
  little modification from you to implement the local operator for
  this equation. {\bf Find the right location to insert your code!}

  Afterwards, you will get a non-linear system of equations in $u$.
  The function {\tt example01b\_Qk(...)} will apply the Newton method
  instead of the {\tt StationaryProblemLinearSolver} from example
  1a. See {\tt example01b\_Qk.hh} for the details.

  {\bf Uncomment the section for exercise 1b inside the file {\tt
      example01\_main.hh} to activate this code.}

  A direct comparison of the outputs of 1b and 1a (both with $Q_1$
  elements, grid $2^{6}\times2^{6}$) is shown in Figure 1.

  \begin{figure}[H]
    \centering
    \begin{minipage}[b]{.6\linewidth}
      \centering \includegraphics[width=\linewidth]{example01b}
      %\caption{\label{fig:heterogoneous}heterogeneous diffusion field}
      %\vspace\baselineskip
    \end{minipage}
    \caption{Solution of 1a (upper surface) and 1b (lower surface)}
  \end{figure}


  \vspace{5mm}
\item {\sc Variable diffusion coefficient}:\\ {\bf Example 1c)} Now,
  we add a structural parameter to our the linear elliptic equation
  from above:
  $$
  \begin{array}{cccc}
    -\mathrm{div} \{ K(\vx) \cdot \grad u \} + a u & = & f & \text{ in }
    \Omega \\
  \end{array}
  $$ with $a$ and $f$ as specified above and the diffusion coefficient
  $K(\vx)$ defined below.  Two new header files
  $$
  \begin{array}{cc}
    {\tt example01c\_Q1.hh} &\hspace{1cm} {\tt example01c\_operator.hh}
  \end{array}
  $$ are provided. {\bf Uncomment the sections for exercise 1c inside
    the file {\tt example01\_main.hh} to activate this code.}\\

  A diffusion coefficient with a checker-board pattern is defined as
  follows:\\

  $$ K(\vx) := \left\{
  \begin{array}{rl}
    2.0 & \mbox{ for $i_1\equiv 0 \wedge i_2\equiv 0$} \\ 0.1 & \mbox{
      for $i_1\equiv 1 \wedge i_2\equiv 0$} \\ 0.5 & \mbox{ for
      $i_1\equiv 0 \wedge i_2\equiv 1$} \\ 1.0 & \mbox{ for $i_1\equiv 1
      \wedge i_2\equiv 1$} \\
  \end{array}
  \right.
  $$ with
  $$
  \begin{array}{rl}
    i_1 & := \lfloor 8 x_1 \rfloor \mod{2}\\ i_2 & := \lfloor 8 x_2
    \rfloor \mod{2}
  \end{array}
  $$

  \newpage

  {\bf Add the implementation of this coefficient into the local
    operator for the diffusion equation!}

  {\it Hints:}\\ Search for the lines with
\begin{verbatim}
// >>> Todo:...
\end{verbatim}
C++ offers the function {\tt double floor(double x)} to calculate
$\lfloor x \rfloor = \mathrm{max}\{n\in \mathbb{Z} | n \leq x \}$ for
$x \in \mathbb{R}$.  The binary {\tt \%}-operator is used to get the
remainder for an integer division.

A solution of 1c is shown in Figure 2.

\begin{figure}[H]
  \centering
  \begin{minipage}[b]{.69\linewidth}
    \centering \includegraphics[width=\linewidth]{example01c}
    %\caption{\label{fig:heterogoneous}heterogeneous diffusion field}
    %\vspace\baselineskip
  \end{minipage}
  \caption{Solution of 1c: heterogeneous diffusion field}
\end{figure}

{\bf Change the values for the diffusion field, for example in the
  following way: }
$$ K(\vx) := \left\{
\begin{array}{rl}
  1.0\times 10^{+9} & \mbox{ for $i_1\equiv 0 \wedge i_2\equiv 0$}
  \\ 1.0\times 10^{-6} & \mbox{ for $i_1\equiv 1 \wedge i_2\equiv 0$}
  \\ 1.0\times 10^{-2} & \mbox{ for $i_1\equiv 0 \wedge i_2\equiv 1$}
  \\ 1.0\times 10^{+5} & \mbox{ for $i_1\equiv 1 \wedge i_2\equiv 1$}
  \\
\end{array}
\right.
$$ with
$$
\begin{array}{rl}
  i_1 & := \lfloor 16 x_1 \rfloor \mod{2}\\ i_2 & := \lfloor 16 x_2
  \rfloor \mod{2}
\end{array}
$$ {\bf See how the solution changes! For a higher resolution
  (e.g. $2^{9}\times2^{9}$) you might try the faster linear solver
  backend }

\begin{verbatim}
typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> LS;
LS ls(5000,1);
\end{verbatim}

\vspace{1cm}

\newpage
\item {\sc A numerical scheme with interior faces}:\\ Consider the
  Laplace equation with Neumann \textit{and Dirichlet} boundary
  conditions.
  $$
  \begin{array}{rclc}
    -\Laplace u + a u & = & q & \text{ in } \Omega \\ u & = & g & \text{
      on } \Gamma_D\\ - \nabla u \cdot n & = & j & \text{ on } \Gamma_N
  \end{array}
  $$ The following example demonstrates the PDELab way of implementing
  the cell-centred finite volume scheme to solve this model problem.


  \textbf{Weak Formulation on the grid:}

  For a grid $E_h^0$, we define $W_h = \{ u \in L_2(\Omega) \,:\,
  u|_{\Omega_e} = \text{const} \ \forall e\in E_h^0\}$ to the be the set
  of cell-wise constant functions. Let $E_h^1$ denote the set of
  interior faces and $B_h^1$ be the set of boundary faces. For an
  interior face $f \in E_h^1$, let $e^-_f$ denote the element to which
  $f$ belongs. Then, $e^+_f$ denotes the neighbouring element on the
  other side of $f$.  The unit normal vector $n_f$ is pointing from
  $e^-_f$ to $e^+_f$. \\ $x_f^-$ is the center of $e_f^-$ and $x_f^+$ is
  the center of $e_f^+$. \\ $[v]|_f = v(x_f^-) - v(x_f^+)$ is the jump
  of $v$ on the face $f$. \\

  Find $u\in W_h$ such that
  \begin{equation*}
    \begin{split}
      & r(u,v) = \sum_{e\in E_h^0} \int_{\Omega_e} (-\Delta u + au - q) v \,
      dx \\ & \quad = \sum_{e\in E_h^0} \biggl\{ \int_{\Omega_e} (au - q)
      v \, dx - \int_{\partial\Omega_e} (\nabla u\cdot n) v \, ds
      \biggr\}, \hspace{5mm} \text{since } u|_{\Omega_e} = \text{ const}
      \\ & \quad = \sum_{e\in E_h^0} \biggl\{ \int_{\Omega_e} (au - q) v
      \, dx - \int_{\partial\Omega_e\cap\Omega} (\nabla u\cdot n) v \, ds
      - \int_{\partial\Omega_e\cap\Gamma_D} (\nabla u\cdot n) v \, ds +
      \int_{\partial\Omega_e\cap\Gamma_N} j v \, ds \biggr\}\\ &\quad =
      \sum_{e\in E_h^0} \int_{\Omega_e} (au - q) v \, dx - \sum_{f\in
        E_h^1} \int_{\Omega_f} (\nabla u\cdot n_f) [v] \, ds - \sum_{f\in
        B_h^1} \int_{\Omega_f\cap\Gamma_D} (\nabla u\cdot n) v \, ds +
      \sum_{f\in B_h^1} \int_{\Omega_f\cap\Gamma_N} j v \, ds \\ &\forall
      v\in W_h.
    \end{split}
  \end{equation*}


  \textbf{Approximation with difference quotient and midpoint rule:}
  \begin{equation*}
    \begin{split}
      r_h^{\text{FV}}(u_h,v) & = \sum_{e\in E_h^0} \bigl(a(x_e)u_h(x_e) -
      q(x_e)\bigr) v(x_e) |\Omega_e|\\ &\qquad - \sum_{f\in E_h^1}
      \frac{u_h(x_f^+)-u_h(x_f^-)}{\|x_f^+ - x_f^-\|} \bigl(v(x_f^-) -
      v(x_f^+)\bigr) |\Omega_f|\\ &\qquad - \sum_{f\in B_h^1;
        \Omega_f\subseteq\Gamma_D} \frac{g(x_f)-u_h(x_f^-)}{\|x_f - x_f^-\|}
      v(x_f^-) |\Omega_f|\\ &\qquad + \sum_{f\in B_h^1;
        \Omega_f\subseteq\Gamma_N} j(x_f) v(x_f) |\Omega_f| .
    \end{split}
  \end{equation*}
  The discrete problem then reads
  \begin{equation*}
    u_h \in W_h : \quad r_h^{\text{FV}}(u_h,v) = 0 \qquad \forall v \in
    W_h.
  \end{equation*}

  Get yourself acquainted with the discrete weak formulation of the
  problem and find out from the source code how the boundary conditions
  look like.  {\bf Try to add the missing sections in the local operator
    yourself:} The interior face term for $f\in E_h^1$ has to be
  implemented in the new method \texttt{alpha\_skeleton()}.

  The source files for this example are
  $$
  \begin{array}{ccc}
    {\tt example04.cc} &\hspace{1cm} {\tt example04.hh} &\hspace{1cm} {\tt
      example04\_operator.hh}
  \end{array}
  $$


  \end{enumerate}
\end{uebung}




\end{document}
