template<int k, class GV>
void example01b_Qk (const GV& gv)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,k> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.name("solution");
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;

  // <<<3>>> Make grid operator
  typedef Example01bLocalOperator LOP;                                 // <= NEW
  LOP lop(2*k);
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  // Structured 2D grid, Q1 finite elements => 9-point stencil / Q2 => 25
  MBE mbe(k == 1 ? 9 : 25);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,gfs,lop,mbe);

  // <<<4>>> Select a linear solver backend
  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  LS ls(5000,true);

  // <<<5>>> solve nonlinear problem
  typedef typename GO::Traits::Domain U;
  U u(gfs,2.0); // initial value
  Dune::PDELab::Newton<GO,LS,U> newton(go,u,ls);                         // <= NEW
  newton.setReassembleThreshold(0.0);
  newton.setVerbosityLevel(2);
  newton.setReduction(1e-10);
  newton.setMinLinearReduction(1e-4);
  newton.setMaxIterations(25);
  newton.setLineSearchMaxIterations(10);
  newton.apply();

  // <<<6>>> graphical output
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,k == 1 ? 0 : 3);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
  std::stringstream basename;
  basename << "example01b_Q" << k;
  vtkwriter.write(basename.str(),Dune::VTK::appendedraw);

}
