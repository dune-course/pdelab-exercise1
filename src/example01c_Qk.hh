template<int k, class GV>
void example01c_Qk (const GV& gv)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,k> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.name("solution");
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;

  // <<<3>>> Make grid operator
  typedef Example01cLocalOperator LOP;
  LOP lop(2*k);
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  // Structured 2D grid, Q1 finite elements => 9-point stencil / Q2 => 25
  MBE mbe(k == 1 ? 9 : 25);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,gfs,lop,mbe);

  // How well did we estimate the number of entries per matrix row?
  // => print Jacobian pattern statistics
  typename GO::Traits::Jacobian jac(go);
  std::cout << jac.patternStatistics() << std::endl;

  // <<<4>>> Select a linear solver backend
  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  LS ls(5000,true);

  // <<<5>>> solve linear problem
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0); // initial value
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,ls,u,1e-10);
  slp.apply();

  // <<<6>>> graphical output
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,1);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
  std::stringstream basename;
  basename << "example01c_Q" << k;
  vtkwriter.write(basename.str(),Dune::VTK::appendedraw);
}
